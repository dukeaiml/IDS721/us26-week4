# Us26 Week4

### Containerizing a Rust Actix Web Service
This project aims to containerize a Rust Actix web service that generates a random message when the  page is refreshed.

### Packages and Dependencies
To install Docker:

1 - Go to the official Docker website: [Docker Official Website.](https://docs.docker.com/desktop/)

2 - Follow the instructions provided to download and install Docker for your operating system.

### Build and Deploy the Project

Follow these steps to build and deploy the project:

1 - Run the following command to create a new blank Rust project:

    cargo new week4_us26

2 - Update the code in the /src/main.rs file to add functionalities to the Rust function.

3 - Add dependencies to the Cargo.toml file.

    actix-web = "4" and rand = "0.8"

4 - Run the following command to test whether the main function works locally:

    cargo run

5 - Open the Docker console and ensure that Docker Desktop is running.

6 - Write the Dockerfile to specify the steps including FROM, WORKDIR, USER, COPY, RUN, EXPOSE, and CMD.

7 - Build the Docker image by running the following command in the terminal:

    docker build -t week4_us26 .

8 - Run the Docker container by executing the following command:

    docker run -p 8080:8080 week4_us26

9 - Running the Container
After building the Docker image, run the container using the following command:

    docker run -p 8080:8080 week4_us26

10 - Access the Actix web service by navigating to http://localhost:8080 in your web browser.


### Results
    docker build -t week4_us26 .
![Screenshot](screenshots/1.png)

    docker run -p 8080:8080 week4_us26
![Screenshot](screenshots/2.png)

![Screenshot](screenshots/3.png)

    http://localhost:8080 in your web browser.
![Screenshot](screenshots/4.png)
